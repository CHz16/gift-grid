extends "res://grid.gd"


const CheckFlag := preload("res://checks.gd").CheckFlag


signal changed


var is_drawing := false
var drawing_state: int
var undo_stack := []
var redo_stack := []
var flashing_cells := []
var disabled := false


func undo() -> void:
	if undo_stack.empty():
		return
	var modification: Modification = undo_stack.pop_back()
	var inverse_modification := _make_inverse_modification(modification)
	_apply_modification(modification)
	redo_stack.append(inverse_modification)
	emit_signal("changed")

func redo() -> void:
	if redo_stack.empty():
		return
	var modification: Modification = redo_stack.pop_back()
	var inverse_modification := _make_inverse_modification(modification)
	_apply_modification(modification)
	undo_stack.append(inverse_modification)
	emit_signal("changed")

func erase() -> void:
	var clear_coords := []
	var clear_shadings := []
	for coords in shadings:
		if shadings[coords] != CellState.UNKNOWN:
			clear_coords.append(coords)
			clear_shadings.append(shadings[coords])
			shadings[coords] = CellState.UNKNOWN
			cells[coords].set_state(CellState.UNKNOWN)
	if !clear_coords.empty():
		undo_stack.append(Modification.new(clear_coords, clear_shadings))
		redo_stack = []
		emit_signal("changed")


func disable() -> void:
	disabled = true


func check(flags: int) -> int:
	# Answer check priorities, with why I spontaneously ordered them this way:
	# --- Always an error ---
	# 1). Disconnected unshaded areas
	#     Unequivocally a major error, good chance of not being fixable easily
	# 2-t). Too many shaded cells in a region
	# 2-t). Too few shaded cells in a region without enough unmarked cells to reach the count
	#       Definitely major errors, higher chance of possibly being able to just
	#       mark an alternate cell
	# 3). Nonrectangular shaded mass with an unshaded cell that needs to be shaded
	#     Strikes me as the easiest of the confirmed errors to be a random mismark
	# --- Potentially an error ---
	# 4). Nonrectangular shaded mass that could potentially be satisfied
	#     Higher priority because these are free marks
	# 5). Too few shaded cells in a region that could potentially be satisfied
	#     Bottom tier because this is probably not helpful
	var region_checks := { "error": [], "potential": [] }
	var rectangle_checks := { "error": [], "potential": [] }
	
	if flags & CheckFlag.CONNECTED:
		var connected_clumps := _find_clumps([CellState.UNKNOWN, CellState.UNSHADED])
		if len(connected_clumps) > 1:
			# We'll just flash the smallest of the unshaded areas
			var smallest_clump := 0
			for i in range(1, len(connected_clumps)):
				if len(connected_clumps[i]) < len(connected_clumps[smallest_clump]):
					smallest_clump = i
			_flash(connected_clumps[smallest_clump])
			return CheckFlag.CONNECTED
	
	if flags & CheckFlag.REGIONS:
		region_checks = _check_regions()
		if !region_checks["error"].empty():
			_flash(region_checks["error"])
			return CheckFlag.REGIONS
	
	if flags & CheckFlag.RECTANGLES:
		rectangle_checks = _check_rectangles()
		if !rectangle_checks["error"].empty():
			_flash(rectangle_checks["error"])
			return CheckFlag.RECTANGLES
	
	if !rectangle_checks["potential"].empty():
		_flash(rectangle_checks["potential"])
		return CheckFlag.RECTANGLES
	
	if !region_checks["potential"].empty():
		_flash(region_checks["potential"])
		return CheckFlag.REGIONS
	
	return 0

func _find_clumps(states: Array) -> Array:
	var visited := {}
	for row in range(number_of_rows):
		for col in range(number_of_cols):
			var coords := Vector2(row, col)
			visited[coords] = !states.has(shadings[coords])
	var clumps := []
	for row in range(number_of_rows):
		for col in range(number_of_cols):
			var start := Vector2(row, col)
			if visited[start]:
				continue
			var clump := []
			var stack := [start]
			while !stack.empty():
				var coords: Vector2 = stack.pop_back()
				if visited[coords]:
					continue
				visited[coords] = true
				clump.append(coords)
				if coords.x > 0:
					stack.append(coords + Vector2(-1, 0))
				if coords.x < number_of_rows - 1:
					stack.append(coords + Vector2(1, 0))
				if coords.y > 0:
					stack.append(coords + Vector2(0, -1))
				if coords.y < number_of_cols - 1:
					stack.append(coords + Vector2(0, 1))
			clumps.append(clump)
	return clumps

func _check_regions() -> Dictionary:
	var error := []
	var potential := []
	for i in regions:
		var shaded_count := 0
		var unknown_count := 0
		for coords in regions[i]:
			if shadings[coords] == CellState.SHADED:
				shaded_count += 1
			elif shadings[coords] == CellState.UNKNOWN:
				unknown_count += 1
		var target := int(clue_for_region.get(i, -1))
		if error.empty():
			if target == -1:
				if shaded_count == 0 and unknown_count == 0:
					error = regions[i]
			else:
				if shaded_count > target or shaded_count + unknown_count < target:
					error = regions[i]
		if potential.empty():
			if (target == -1 and shaded_count == 0) or shaded_count < target:
				potential = regions[i]
		if !error.empty() and !potential.empty():
			break
	return { "error": error, "potential": potential }

func _check_rectangles() -> Dictionary:
	var error := []
	var potential := []
	var shaded_clumps := _find_clumps([CellState.SHADED])
	for i in range(len(shaded_clumps)):
		var clump: Array = shaded_clumps[i]
		var extents := _extents_of_clump(clump)
		if (extents.right_edge - extents.left_edge + 1) * (extents.bottom_edge - extents.top_edge + 1) == len(clump):
			continue
		var invalid := false
		for row in range(extents.top_edge, extents.bottom_edge + 1):
			for col in range(extents.left_edge, extents.right_edge + 1):
				if shadings[Vector2(row, col)] == CellState.UNSHADED:
					invalid = true
					break
			if invalid:
				break
		if invalid and error.empty():
			error = clump
		elif !invalid and potential.empty():
			potential = clump
		if !error.empty() and !potential.empty():
			break
	return { "error": error, "potential": potential }

func _extents_of_clump(clump: Array) -> Dictionary:
	var left_edge := number_of_cols - 1
	var right_edge := 0
	var top_edge := number_of_rows - 1
	var bottom_edge := 0
	for coords in clump:
		left_edge = int(min(left_edge, coords.y))
		right_edge = int(max(right_edge, coords.y))
		top_edge = int(min(top_edge, coords.x))
		bottom_edge = int(max(bottom_edge, coords.x))
	return { "left_edge": left_edge, "right_edge": right_edge, "top_edge": top_edge, "bottom_edge": bottom_edge}

func _flash(cells: Array) -> void:
	stop_flash()
	flashing_cells = cells
	_on_FlashTimer_timeout()
	$FlashTimer.start()

func _on_FlashTimer_timeout():
	for coords in flashing_cells:
		cells[coords].toggle_flash()

func stop_flash() -> void:
	$FlashTimer.stop()
	for coords in flashing_cells:
		cells[coords].unflash()
	flashing_cells = []


func find_rectangles() -> Array:
	var rectangles := []
	for clump in _find_clumps([CellState.SHADED]):
		var extents := _extents_of_clump(clump)
		rectangles.append(Rect2(
			10 * extents.left_edge,
			10 * extents.top_edge,
			10 * (extents.right_edge - extents.left_edge + 1),
			10 * (extents.bottom_edge - extents.top_edge + 1)
		))
	return rectangles


func load_data(data: String) -> void:
	var dict: Dictionary = parse_json(data)
	for row in range(number_of_rows):
		for col in range(number_of_cols):
			var coords := Vector2(row, col)
			var state := int(dict["grid"][row][col])
			shadings[coords] = state
			cells[coords].set_state(state)
	undo_stack = []
	for modification_dict in dict["undo_stack"]:
		var modification := Modification.new([], [])
		modification.load_dict(modification_dict)
		undo_stack.append(modification)
	for modification_dict in dict["redo_stack"]:
		var modification := Modification.new([], [])
		modification.load_dict(modification_dict)
		redo_stack.append(modification)
	emit_signal("changed")

func get_data() -> String:
	var state_grid := []
	for row in range(number_of_rows):
		var state_row = []
		for col in range(number_of_cols):
			state_row.append(shadings[Vector2(row, col)])
		state_grid.append(state_row)
	var serialized_undo_stack := []
	for modification in undo_stack:
		serialized_undo_stack.append(modification.make_dict())
	var serialized_redo_stack := []
	for modification in redo_stack:
		serialized_redo_stack.append(modification.make_dict())
	return to_json({
		"grid": state_grid,
		"undo_stack": serialized_undo_stack,
		"redo_stack": serialized_redo_stack,
	 })



func _input(event: InputEvent) -> void:
	if disabled:
		return
	if event is InputEventMouseButton:
		if !event.is_pressed():
			is_drawing = false
			return
		if get_rect().has_point(event.position) and !is_drawing:
			if event.button_index != BUTTON_LEFT and event.button_index != BUTTON_RIGHT:
				return 
			is_drawing = true
			drawing_state = _next_state_for_button(_coords_for_position(event.position), event.button_index)
			_paint(event.position)
	elif event is InputEventMouseMotion:
		if !is_drawing:
			return
		_paint(event.position)


func _next_state_for_button(coords: Vector2, button: int) -> int:
	if button == BUTTON_LEFT:
		match shadings[coords]:
			CellState.UNKNOWN:
				return CellState.SHADED
			CellState.SHADED:
				return CellState.UNSHADED
			CellState.UNSHADED:
				return CellState.UNKNOWN
	else:
		match shadings[coords]:
			CellState.UNKNOWN:
				return CellState.UNSHADED
			CellState.SHADED:
				return CellState.UNSHADED
			CellState.UNSHADED:
				return CellState.UNKNOWN
	return CellState.UNKNOWN


func _paint(mouse_position: Vector2) -> void:
	if !get_rect().has_point(mouse_position):
		return
	var coords := _coords_for_position(mouse_position)
	if shadings[coords] != drawing_state:
		undo_stack.append(Modification.new([coords], [shadings[coords]]))
		redo_stack = []
		shadings[coords] = drawing_state
		cells[coords].set_state(drawing_state)
		emit_signal("changed")


func _coords_for_position(position: Vector2) -> Vector2:
	var relative_position := position - get_rect().position
	var coords := (Vector2(relative_position.y, relative_position.x) / 10).floor()
	coords.x = clamp(coords.x, 0, number_of_rows - 1)
	coords.y = clamp(coords.y, 0, number_of_cols - 1)
	return coords


func _apply_modification(modification: Modification) -> void:
	for i in range(0, len(modification.coords)):
		shadings[modification.coords[i]] = modification.shadings[i]
		cells[modification.coords[i]].set_state(modification.shadings[i])


func _make_inverse_modification(modification: Modification) -> Modification:
	var old_shadings := []
	for i in range(0, len(modification.coords)):
		old_shadings.append(shadings[modification.coords[i]])
	return Modification.new(modification.coords, old_shadings)



class Modification:
	var coords: Array
	var shadings: Array

	func _init(in_coords: Array, in_shadings: Array) -> void:
		coords = in_coords
		shadings = in_shadings
	
	func load_dict(dict: Dictionary) -> void:
		coords = []
		shadings = []
		for i in range(len(dict["rows"])):
			coords.append(Vector2(int(dict["rows"][i]), int(dict["cols"][i])))
			shadings.append(int(dict["shadings"][i]))
	
	func make_dict() -> Dictionary:
		var rows := []
		var cols := []
		for coord in coords:
			rows.append(coord.x)
			cols.append(coord.y)
		return { "rows": rows, "cols": cols, "shadings": shadings }
