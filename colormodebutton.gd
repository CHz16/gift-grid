extends Node2D


signal pressed


const CellState := preload("cell.gd").CellState


var high_contrast := false


func _ready():
	$UnknownCell/Cell.remove_from_group("cells")
	$UnknownCell/Cell.set_state(CellState.UNKNOWN)
	$UnshadedCell/Cell.remove_from_group("cells")
	$UnshadedCell/Cell.set_state(CellState.UNSHADED)
	$ShadedCell/Cell.remove_from_group("cells")
	$ShadedCell/Cell.set_state(CellState.SHADED)
	_set_icon_contrasts()


func _set_icon_contrasts() -> void:
	$UnknownCell/Cell.set_high_contrast(!high_contrast)
	$UnshadedCell/Cell.set_high_contrast(!high_contrast)
	$ShadedCell/Cell.set_high_contrast(!high_contrast)


func disable() -> void:
	$TextureButton.disabled = true


func _on_TextureButton_pressed():
	high_contrast = !high_contrast
	_set_icon_contrasts()
	emit_signal("pressed", high_contrast)
