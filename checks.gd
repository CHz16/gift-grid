extends MarginContainer


const CellState := preload("cell.gd").CellState


signal check_requested(flags)


enum CheckFlag { REGIONS = 1, RECTANGLES = 2, CONNECTED = 4 }


func _ready() -> void:
	$MarginContainer/MarginContainer/VBoxContainer/Regions.load_examples("regions")
	$MarginContainer/MarginContainer/VBoxContainer/Rectangles.load_examples("rectangles")
	$MarginContainer/MarginContainer/VBoxContainer/Connected.load_examples("connected")
	$MarginContainer/MarginContainer/Annotations/RegionsCell/Cell.set_state(CellState.SHADED)
	$MarginContainer/MarginContainer/Annotations/RectanglesCell/Cell.set_state(CellState.SHADED)
	$MarginContainer/MarginContainer/Annotations/ConnectedCell/Cell.set_state(CellState.UNSHADED)


func highlight_check(flag: int) -> void:
	$MarginContainer/MarginContainer/VBoxContainer/Regions.set_highlight(flag == CheckFlag.REGIONS)
	$MarginContainer/MarginContainer/VBoxContainer/Rectangles.set_highlight(flag == CheckFlag.RECTANGLES)
	$MarginContainer/MarginContainer/VBoxContainer/Connected.set_highlight(flag == CheckFlag.CONNECTED)


func disable() -> void:
	$MarginContainer/MarginContainer/VBoxContainer/Regions.disable()
	$MarginContainer/MarginContainer/VBoxContainer/Rectangles.disable()
	$MarginContainer/MarginContainer/VBoxContainer/Connected.disable()
	$MarginContainer/MarginContainer/VBoxContainer/FullCheck.disabled = true


func _on_Regions_pressed() -> void:
	emit_signal("check_requested", CheckFlag.REGIONS)

func _on_Rectangles_pressed() -> void:
	emit_signal("check_requested", CheckFlag.RECTANGLES)

func _on_Connected_pressed() -> void:
	emit_signal("check_requested", CheckFlag.CONNECTED)

func _on_FullCheck_pressed() -> void:
	emit_signal("check_requested", CheckFlag.REGIONS | CheckFlag.RECTANGLES | CheckFlag.CONNECTED)
