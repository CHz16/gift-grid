extends ColorRect


const Cell := preload("cell.tscn")
const CellState := preload("cell.gd").CellState


var number_of_rows: int
var number_of_cols: int
var regions := {}
var cells := {}
var region_for_cell := {}
var clue_for_region := {}
var shadings := {}


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	add_to_group("grids")
	set_high_contrast(false)


func load_puzzle(path: String) -> void:
	var file := File.new()
	var _error := file.open(path, File.READ)
	number_of_rows = int(file.get_line())
	
	# Regions
	for row in range(0, number_of_rows):
		var cell_tags := file.get_line().split(" ", false)
		if row == 0:
			number_of_cols = len(cell_tags)
			$Cells.set_columns(len(cell_tags))
			$GridLines.set_columns(len(cell_tags) - 1)
		for col in range(0, len(cell_tags)):
			var coords := Vector2(row, col)
			region_for_cell[coords] = cell_tags[col]
			regions[cell_tags[col]] = regions.get(cell_tags[col], []) + [coords]
			var cell := Cell.instance()
			cells[coords] = cell
			$Cells.add_child(cell)
			if row > 0 and col > 0:
				var region_lines := 0
				var top_line := false
				var bottom_line := false
				var left_line := false
				var right_line := false
				if region_for_cell[Vector2(row - 1, col - 1)] != region_for_cell[Vector2(row - 1, col)]:
					top_line = true
					region_lines += 1
				if region_for_cell[Vector2(row, col - 1)] != region_for_cell[Vector2(row, col)]:
					bottom_line = true
					region_lines += 1
				if region_for_cell[Vector2(row - 1, col - 1)] != region_for_cell[Vector2(row, col - 1)]:
					left_line = true
					region_lines += 1
				if region_for_cell[Vector2(row - 1, col)] != region_for_cell[Vector2(row, col)]:
					right_line = true
					region_lines += 1
				var image_name := "res://images/gridlines" + str(region_lines)
				var flip_h := false
				var flip_v := false
				if region_lines == 2:
					if top_line and right_line:
						image_name += "-l"
					elif top_line and left_line:
						image_name += "-l"
						flip_h = true
					elif bottom_line and left_line:
						image_name += "-l"
						flip_h = true
						flip_v = true
					elif bottom_line and right_line:
						image_name += "-l"
						flip_v = true
					elif top_line:
						image_name += "-v"
					elif left_line:
						image_name += "-h"
				elif region_lines == 3:
					if !bottom_line:
						image_name += "-v"
					elif !top_line:
						image_name += "-v"
						flip_v = true
					elif !right_line:
						image_name += "-h"
						flip_h = true
					elif !left_line:
						image_name += "-h"
				var n := TextureRect.new()
				n.set_flip_h(flip_h)
				n.set_flip_v(flip_v)
				n.set_texture(load(image_name + ".png"))
				$GridLines.add_child(n)
	
	# Clues
	for row in range(0, number_of_rows):
		var clues := file.get_line().split(" ", false)
		for col in range(0, len(clues)):
			if clues[col] != ".":
				var coords := Vector2(row, col)
				cells[coords].set_clue(clues[col])
				clue_for_region[region_for_cell[coords]] = clues[col]
	
	# Pre-existing shading
	for row in range(0, number_of_rows):
		var shades := file.get_line().split(" ", false)
		for col in range(0, len(shades)):
			var coords := Vector2(row, col)
			match shades[col]:
				".":
					shadings[coords] = CellState.UNKNOWN
				"*":
					shadings[coords] = CellState.SHADED
				"-":
					shadings[coords] = CellState.UNSHADED
			cells[coords].set_state(shadings[coords])
	
	rect_min_size = Vector2(number_of_cols * 10 + 1, number_of_rows * 10 + 1)
	$InternalGridLines.rect_min_size = rect_min_size - Vector2(2, 2)
	file.close()


func set_high_contrast(high_contrast: bool) -> void:
	$InternalGridLines.color = Color("#c0c0c0") if high_contrast else Color("be7dbc")
