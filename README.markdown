Gift Grid
=========

A very mildly original logic (pen and paper style) puzzle, made for [The Confounding Calendar](https://confoundingcalendar.itch.io/). Playable here: https://chz.itch.io/gift-grid

This is a [Godot project](https://godotengine.org).

Everything here is by me and released under MIT (see LICENSE.txt).
