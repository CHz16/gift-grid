extends ParallaxBackground


export var scroll_speed := Vector2(1.0, 1.0)


func _process(delta: float) -> void:
	var new_offset := get_scroll_offset() + scroll_speed * delta
	set_scroll_offset(new_offset)
