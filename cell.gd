extends ColorRect


enum CellState { UNKNOWN, SHADED, UNSHADED }


var state: int = CellState.UNKNOWN
var high_contrast := false


func _ready() -> void:
	add_to_group("cells")
	_set_cell_color()


func set_clue(clue: String) -> void:
	$Clue.set_texture(load("res://images/clue" + clue + ".png"))

func set_state(in_state: int) -> void:
	state = in_state
	_set_cell_color()

func set_high_contrast(in_high_contrast: bool) -> void:
	high_contrast = in_high_contrast
	_set_cell_color()


func _set_cell_color() -> void:
	match state:
		CellState.UNKNOWN:
			color = Color("#ffffff") if high_contrast else Color("#922a95")
		CellState.SHADED:
			color = Color("#707070") if high_contrast else Color("#7f6962")
		CellState.UNSHADED:
			color = Color("#b0ffb0") if high_contrast else Color("#f9cb60")


func toggle_flash() -> void:
	$Flash.visible = !$Flash.visible

func unflash() -> void:
	$Flash.visible = false
