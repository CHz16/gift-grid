extends Control


enum Palette { PURPLE, RED, ORANGE, DARK }


const BOX_COLORS := {
	Palette.PURPLE: { "outline": "#60117f", "top": "#be7dbc" },
	Palette.RED: { "outline": "#680703", "top": "#bc2f01" },
	Palette.ORANGE: { "outline": "#f9960f", "top": "#f9cb60" },
	Palette.DARK: { "outline": "#151244", "top": "#7f6962" },
}

const RIBBON_COLORS := {
	Palette.PURPLE: { "ribbon": "#60117f", "bow": "#f9960f" },
	Palette.RED: { "ribbon": "#bc2f01", "bow": "#60117f" },
	Palette.ORANGE: { "ribbon": "#f9960f", "bow": "#60117f" },
	Palette.DARK: { "ribbon": "#151244", "bow": "#f9960f" },
}

const BOW_SHAPES := [
	[Vector2(0, 0), Vector2(1, 0), Vector2(0, 1), Vector2(1, 1)],
	[Vector2(-1, 0), Vector2(-1, 1), Vector2(0, 2), Vector2(1, 2), Vector2(2, 0), Vector2(2, 1), Vector2(0, -1), Vector2(1, -1)],
	[Vector2(0, 0), Vector2(1, 0), Vector2(0, 1), Vector2(1, 1), Vector2(-1, 0), Vector2(-1, 1), Vector2(0, 2), Vector2(1, 2), Vector2(2, 0), Vector2(2, 1), Vector2(0, -1), Vector2(1, -1)],
	[Vector2(-1, -1), Vector2(0, 0), Vector2(1, 1), Vector2(2, 2), Vector2(-1, 2), Vector2(0, 1), Vector2(1, 0), Vector2(2, -1)],
]


var height: int
var box_palette: int
var ribbon_palette: int
var bow: int


func _init() -> void:
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	height = rng.randi_range(1, 4)
	box_palette = _random_palette(rng)
	ribbon_palette = box_palette
	while ribbon_palette == box_palette:
		ribbon_palette = _random_palette(rng)
	bow = rng.randi_range(0, len(BOW_SHAPES) - 1)


func _draw() -> void:
	var box_points = PoolVector2Array([
		Vector2(-height, -height),
		Vector2(rect_size.x - height, -height),
		Vector2(rect_size.x, 0),
		Vector2(rect_size.x, rect_size.y),
		Vector2(1, rect_size.y),
		Vector2(-height, rect_size.y - height),
	])
	draw_colored_polygon(box_points, BOX_COLORS[box_palette]["outline"])
	draw_rect(Rect2(-height + 1, -height + 1, rect_size.x - 2, rect_size.y - 2), BOX_COLORS[box_palette]["top"])
	
	var vert_ribbon_points = PoolVector2Array([
		Vector2(rect_size.x / 2 - 1 - height, -height),
		Vector2(rect_size.x / 2 + 1 - height, -height),
		Vector2(rect_size.x / 2 + 1 - height, rect_size.y - height),
		Vector2(rect_size.x / 2 + 2, rect_size.y),
		Vector2(rect_size.x / 2, rect_size.y),
		Vector2(rect_size.x / 2 - 1 - height, rect_size.y - height),
	])
	var horiz_ribbon_points = PoolVector2Array([
		Vector2(-height, rect_size.y / 2 - 1 - height),
		Vector2(-height, rect_size.y / 2 + 1 - height),
		Vector2(rect_size.x - height, rect_size.y / 2 + 1 - height),
		Vector2(rect_size.x, rect_size.y / 2 + 1),
		Vector2(rect_size.x, rect_size.y / 2),
		Vector2(rect_size.x - height, rect_size.y / 2 - 1 - height),
	])
	draw_colored_polygon(vert_ribbon_points, RIBBON_COLORS[ribbon_palette]["ribbon"])
	draw_colored_polygon(horiz_ribbon_points, RIBBON_COLORS[ribbon_palette]["ribbon"])
	
	for point in BOW_SHAPES[bow]:
		draw_rect(Rect2(rect_size.x / 2 - height - 1 + point.x, rect_size.y / 2 - height - 1 + point.y, 1, 1), RIBBON_COLORS[ribbon_palette]["bow"])


func _random_palette(rng: RandomNumberGenerator) -> int:
	return Palette.values()[rng.randi_range(0, Palette.size() - 1)]
