extends MarginContainer


signal undo
signal redo
signal erase


func disable() -> void:
	$MarginContainer/MarginContainer/SolveButtons/Undo.disabled = true
	$MarginContainer/MarginContainer/SolveButtons/Redo.disabled = true
	$MarginContainer/MarginContainer/SolveButtons/Erase.disabled = true


func _on_Undo_pressed() -> void:
	emit_signal("undo")


func _on_Redo_pressed() -> void:
	emit_signal("redo")


func _on_Erase_pressed() -> void:
	emit_signal("erase")
