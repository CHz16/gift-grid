extends MarginContainer


signal pressed


func load_examples(name: String) -> void:
	$MarginContainer/MarginContainer/HBoxContainer/IncorrectExample.load_puzzle("res://puzzles/" + name + "-incorrect.txt")
	$MarginContainer/MarginContainer/HBoxContainer/CorrectExample.load_puzzle("res://puzzles/" + name + "-correct.txt")

func set_highlight(highlight: bool) -> void:
	$ColorRect.color = "#151244" if highlight else "#922a95"
	$MarginContainer/ColorRect.color = "#f9960f" if highlight else "#922a95"


func disable() -> void:
	$Button.disabled = true


func _on_Button_pressed() -> void:
	emit_signal("pressed")
