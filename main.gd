extends Node2D


const Present := preload("present.tscn")

const PREFS_FILE := "user://prefs.cfg"
const PREFS_SECTION := "prefs"
const HIGH_CONTRAST_KEY := "high_contrast"

const AUTOSAVE_FILE := "user://autosave.json"


var config: ConfigFile


func _ready() -> void:
	$Grid.load_puzzle("res://puzzles/puzzle.txt")
	var _error := $Grid.connect("changed", self, "_on_Grid_changed")
	config = ConfigFile.new()
	_error = config.load(PREFS_FILE)
	if config.get_value(PREFS_SECTION, HIGH_CONTRAST_KEY, false):
		$ColorModeButton._on_TextureButton_pressed()
	var autosave_file := File.new()
	if autosave_file.file_exists(AUTOSAVE_FILE):
		_error = autosave_file.open(AUTOSAVE_FILE, File.READ)
		var autosave = autosave_file.get_line()
		$Grid.load_data(autosave)
		autosave_file.close()


func _input(event: InputEvent) -> void:
	if !$Grid/FlashTimer.is_stopped() and event is InputEventMouseButton:
		$Grid.stop_flash()
		$Checks.highlight_check(0)


func _win() -> void:
	# 1). Delete the autosave.
	var dir := Directory.new()
	var _error := dir.remove(AUTOSAVE_FILE)
	
	# 2). Disable all input and slide the grid center and the controls off screen
	$Grid.disable()
	$Controls.disable()
	$Checks.disable()
	$ColorModeButton.disable()
	$AnimationPlayer.play("win", -1, 1.2)
	yield($AnimationPlayer, "animation_finished")
	
	# 3). Create and drop some random presents.
	for rectangle in $Grid.find_rectangles():
		var destination: Vector2 = rectangle.position + $Grid.rect_position
		var offset: float = destination.y + rectangle.size.y
		var start: Vector2 = destination - Vector2(offset, offset)
		var present := Present.instance()
		present.set_position(start)
		present.set_size(rectangle.size)
		add_child(present)
		var _result = $Tween.interpolate_property(present, "rect_position", null, destination, 0.5, Tween.TRANS_QUAD)
		_result = $Tween.start()
		yield(get_tree().create_timer(0.1), "timeout")
	yield(get_tree().create_timer(1), "timeout")
	
	# 4). Do some final drawing and drop the grid.
	$EndingBackgroundFrame.set_position($Grid.rect_position)
	$EndingBackgroundFrame.set_size($Grid.rect_size)
	$EndingBackgroundFrame.visible = true
	$EndingBackground.set_position($Grid.rect_position + Vector2(1, 1))
	$EndingBackground.set_size($Grid.rect_size - Vector2(2, 2))
	$EndingBackground.visible = true
	$Credit.visible = true
	$Stars.set_position($Grid.rect_position + $Grid.rect_size / 2)
	$Stars.set_emitting(true)
	move_child($Stars, get_child_count())
	var destination := Vector2($Grid.rect_position.x, get_viewport_rect().size.y)
	var _result = $Tween.interpolate_property($Grid, "rect_position", null, destination, 1, Tween.TRANS_CUBIC, Tween.EASE_IN)
	_result = $Tween.interpolate_property($Grid, "rect_rotation", null, 45, 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	_result = $Tween.start()


func _on_Grid_changed() -> void:
	# lmao
	$Controls/MarginContainer/MarginContainer/SolveButtons/Undo.disabled = $Grid.undo_stack.empty()
	$Controls/MarginContainer/MarginContainer/SolveButtons/Redo.disabled = $Grid.redo_stack.empty()
	var autosave_file := File.new()
	var _error := autosave_file.open(AUTOSAVE_FILE, File.WRITE)
	autosave_file.store_line($Grid.get_data())
	autosave_file.close()


func _on_Controls_undo() -> void:
	$Grid.undo()

func _on_Controls_redo() -> void:
	$Grid.redo()

func _on_Controls_erase() -> void:
	$Grid.erase()


func _on_Checks_check_requested(flags: int) -> void:
	var error_found: int = $Grid.check(flags)
	if flags == 7 and error_found == 0:
		_win()
	else:
		$Checks.highlight_check(error_found)


func _on_ColorModeButton_pressed(high_contrast: bool) -> void:
	get_tree().call_group("cells", "set_high_contrast", high_contrast)
	get_tree().call_group("grids", "set_high_contrast", high_contrast)
	config.set_value(PREFS_SECTION, HIGH_CONTRAST_KEY, high_contrast)
	var _error = config.save(PREFS_FILE)
