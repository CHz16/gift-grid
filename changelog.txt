Version 1.0.1 (2021-12-14)
------------------------

Lightened the color of shaded cells in high contrast mode based on feedback that it was hard to tell where region borders are.



Version 1.0 (2021-11-30)
------------------------

Initial version.
